# modiosSource

Love2D 11.1 iOS source with extended modules.

Implementation taken from:
https://github.com/rameshvarun/fault/tree/master/game

# New Module Functions:

## System:

###

```lua

	local osString = love.system.getOS( )
	
	if(osString == "iOS") then
		love.system.authenticateLocalPlayer()
	end

```
###

Authenticates the player and syncs scores and achievements from GameCenter.
If successful, a welcome banner will appear. *Requires Apple Developer Membership 

###

```lua
	love.system.requestReview()

```

###

Will prompt the user to give a review. Follow the Apple [guidelines](https://developer.apple.com/documentation/storekit/skstorereviewcontroller/requesting_app_store_reviews) for use of this function.

###

```lua

	local id = "leaderboardid"
	
	love.system.showLeaderboard(id)

```

###

Shows the leaderboard View Controller for your game.

###

```lua
	local id = "leaderboardid"
	
	local highscore = love.system.getHighScore(id)

```

###

Returns an existing high score.

###

```lua
	local id = "leaderboardid"
	local highscore = 100000
	
	love.system.submitScore(id, highscore)

```

###

Sends a score to GameCenter.

###
	
## Window:

###

```lua
	local left, top, right, bottom = love.window.getSafeAreaInsets()

```

###

Returns the safe area insets. Useful for adjusting UI for iPhone X.
