//
//  Review.m
//  love-macosx
//
//  Created by Bruce Berrios on 7/29/18.
//

#import "Review.h"
#import <Storekit/Storekit.h>

@implementation Review

+ (Review *)reviewManager {
	static Review *singleton;
	
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		singleton = [[Review alloc] init];
	});
	
	return singleton;
}

- (void)DisplayReviewController {
	if (@available(iOS 10.3, *)) {
		[SKStoreReviewController requestReview];
	}
}

@end
