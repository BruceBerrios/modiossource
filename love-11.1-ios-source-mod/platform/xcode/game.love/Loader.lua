
local Loader = {}

Loader.filesDir = "res.files."
Loader.libDir = "res.lib."
Loader.gfxDir = "res.gfx."
Loader.sfxDir = "res.sfx."
Loader.shadersDir = "res.shaders."

Loader.files = {
  "Game"
}

Loader.sounds = {

}

Loader.music = {

}

Loader.images = {

}

Loader.shaders = {

}

Loader.load = function(self)
  self:loadFiles()
  self:loadSounds()
  self:loadMusic()
  self:loadImages()
  self:loadLib()
  self:loadShaders()
end

Loader.loadFiles = function(self)
  for i = 1, #self.files do
    require(self.filesDir .. self.files[i])
  end
end

Loader.loadSounds = function(self)
end

Loader.loadMusic = function(self)
end

Loader.loadImages = function(self)
end

Loader.loadLib = function(self)
  lume = require(self.libDir .. "lume")
end

Loader.loadShaders = function(self)
end

return Loader
