
math.randomseed(os.time())

IOS = love.system.getOS() == "iOS"

--[[
if IOS then
  left, top, right, bottom = love.window.getSafeAreaInsets()
  love.system.authenticateLocalPlayer()
end
]]--

function love.load ()
  Loader = require("Loader")

  Loader:load()
end

MAX_DELTA_TIME = 1 / 30

function love.update(dt)
  if dt > MAX_DELTA_TIME then dt = MAX_DELTA_TIME end
  Game:update(dt)
end

function love.draw()
  Game:draw()
end

function love.focus(focus)
  Game:focus(focus)
end

function love.touchreleased(id)
  Game:touchReleased(id)
end

function love.quit() -- On quit
  Game:quit()
end
