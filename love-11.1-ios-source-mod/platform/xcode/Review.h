//
//  Review.h
//  love
//
//  Created by Bruce Berrios on 7/29/18.
//

#import <Foundation/Foundation.h>

#ifndef Review_h
#define Review_h

@interface Review : NSObject
+ (Review *)reviewManager;
- (void) DisplayReviewController;
@end

#endif /* Review_h */
